#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() 
{
    int a = 0;
    int b = 5;
    scanf("%d", &a);
    if (a % b == 0)
    {
        printf("YES\n");
    }
    else
    {
        printf("NO\n");
    }
     return 0;
}